
'use strict'

var express = require('express'),
	app = express(),
    bodyParser = require('body-parser'),
    url = require('url'),
	_ = require('lodash'),
	path = require('path'),
    fs = require('fs'),
    async = require('async'),
    category = require('./category.js'),
    gt = require('google-trends-api'),
    googleTrends = require('./trends.js');

app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies
app.use(bodyParser.json());

app.listen(8080,"0.0.0.0");
console.log("Google Trends app started at port 8080");

//var allCategories = category.readAllCategories();
//var categoryKeys = _.keysIn(allCategories);
//console.log(`Cat Keys: ${categoryKeys}`);

var categories_available = {
    "autos & vehicles": 47,
    "business & industrial": 12,
    "food & drink": 71,
    "internet & telecom": 13,
    "shopping": 18
}
// var category = "Autoss & Vehicles";
// if(_.has(categories_available, _.toLower(category))){
//     category = categories_available[_.toLower(category)];
//     console.log(`category found: ${category}`);
// }


//create a GET endpoint to receive request 
app.get('/trends',function(req,res){
    console.log("Request received for Providing google Trends info.");
    try{
        //fetch received data from user
        var url_parts = url.parse(req.url, true);
        var received_params = url_parts.query;
        console.log(`Request Params: ${JSON.stringify(received_params)}`);
        
        //fetch all necessary params
        var keyword_list = received_params.keywords;
        //now prepare a keyword array from all the keywords received
        var keywords = _.split(keyword_list,',');
        //check if the keywords is array or not. If not make it an array with the given value
        if(!(_.isArray(keywords))){
            var list = [];
            list.push(keywords);
            keywords = list;
        }

        /**Keeping the default category to ALL
         * If category is specified in the requested then it'll be updated
         * Else ALL category will be used for API calls
         * */
        var category;
        //console.log(`keywords found: ${keywords}`);
        
        //assign category it's number
        if(_.has(categories_available, _.toLower(category))){
            category = categories_available[_.toLower(category)];
        }else{
            category = 0;
        }
        if(_.has(received_params, "category") && !(received_params.category.trim == "")){
            category = received_params.category;
        }
        //console.log(`Category found: ${category}`);
        //if any state is mentioned
        var state = "";
        if(_.has(received_params, "state") && !(received_params.state.trim == "")){
            state = received_params.state;
        }
        // console.log(`State found: ${state}`);

        var startTime;
        if(_.has(received_params, "startTime")){
            startTime = received_params.startTime;
        }else{
            var nowDate = new Date();
            if(nowDate.getMonth() == 0){
                startTime = (nowDate.getFullYear()-1)+"-12-01";
            }else{
                startTime = nowDate.getFullYear()+"-"+nowDate.getMonth()+"-"+nowDate.getDate();
            }
        }
        // console.log(`Start Time: ${startTime}`);
        
        var endTime;
        if(_.has(received_params, "endTime")){
            endTime = received_params.endTime;
        }else{
            var nowDate = new Date();
            endTime = nowDate.getFullYear()+"-"+(_.toInteger(nowDate.getMonth())+1)+"-"+nowDate.getDate();
        }
        console.log(`Category found: ${category}, State found: ${state}, Start Time: ${startTime}, End Time: ${endTime}`);
        var final_trends_array = {};
        //Now collect data from all Google Trends APIs
        googleTrends.getInterestOverTime(keywords, startTime, endTime, category, state).then(data=>{
            //push the data to final result
            final_trends_array["interestOverTime"] = data;
            //Collect getInterestByRegion data
            googleTrends.getInterestByRegion(keywords, startTime, endTime, category, state).then(data1=>{
                //push the data to final result
                final_trends_array["interestByRegion"] = data1;
                
                //define the array for holding the relatedQueries and Topics
                var RelatedQueriesDataArray = [];
                var RelatedTopicsDataArray = [];
                //For relatedQUeries and Topics, we need to make separate API calls for each keyword
                async.map(keywords, function(keyword, callback){
                    
                    //Collect getrelatedQueries data
                    googleTrends.getRelatedQueries(keyword, startTime, endTime, category, state).then(data2=>{
                       // console.log(JSON.stringify(data1));
                       var result = {};
                       result[keyword] = data2.default;
                       RelatedQueriesDataArray.push(result);

                       if(RelatedQueriesDataArray.length == keywords.length && RelatedTopicsDataArray.length == keywords.length){
                            final_trends_array["relatedQueries"] = RelatedQueriesDataArray;
                            final_trends_array["relatedTopics"] = RelatedTopicsDataArray;
                            //send the response back
                            console.log('Sending response back');
                            res.send(final_trends_array);
                       }
                    }).catch(error=>{
                        console.error(error);
                    })
                    
                    //Collect getrelatedTopics data
                    googleTrends.getRelatedTopics(keyword, startTime, endTime, category, state).then(data3=>{
                        var result = {};
                        result[keyword] = data3.default;
                        RelatedTopicsDataArray.push(result);

                        if(RelatedQueriesDataArray.length == keywords.length && RelatedTopicsDataArray.length == keywords.length){
                            final_trends_array["relatedQueries"] = RelatedQueriesDataArray;
                            final_trends_array["relatedTopics"] = RelatedTopicsDataArray;
                            //send the response back
                            console.log('Sending response back');
                            res.send(final_trends_array);
                       }
                        
                    }).catch(error=>{
                        console.error(error);
                     })
                })
            }).catch(error=>{
                console.error(error);
            })
        }).catch(error=>{
            console.error(error);
        })
        
    }catch(error){
        console.error(error);
    }
    
    //test separate calls

})
// var startTime;
// var nowDate = new Date();
// if(nowDate.getMonth() == 0){
//     startTime = (nowDate.getFullYear()-1)+"-12-01";
// }else{
//     startTime = nowDate.getFullYear()+"-"+nowDate.getMonth()+"-"+nowDate.getDate();
// }
        
// console.log(`Start Time: ${startTime}`);
        
// var endTime;
// var nowDate = new Date();
// endTime = nowDate.getFullYear()+"-"+(_.toInteger(nowDate.getMonth())+1)+"-"+nowDate.getDate();
// console.log(`End Time: ${endTime}`);

//Collect getrelatedQueries data
// gt.relatedQueries({keyword: ['central intelligence'], startTime: new Date("2018-03-01"), endTime: new Date("2018-04-01"), geo: "US", category : 0}).then(data2=>{
    
//     console.log(data2);
//     // var result = {};
//     // result[keyword] = data2.default;
//     // RelatedQueriesDataArray.push(result);

//     // if(RelatedQueriesDataArray.length == keywords.length && RelatedTopicsDataArray.length == keywords.length){
//     //      final_trends_array["relatedQueries"] = RelatedQueriesDataArray;
//     //      final_trends_array["relatedTopics"] = RelatedTopicsDataArray;
//     //      //send the response back
//     //      console.log('Sending response back');
//     //      res.send(final_trends_array);
//     // }
//  }).catch(error=>{
//      console.error(error);
//  })