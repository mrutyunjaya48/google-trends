
'use strict';

var googleTrends = require('google-trends-api'),
    Promise = require('promise'),
    _ = require('lodash');

function Trends(){
    //Interest By Region
    var getInterestByRegion = function(keywords, startTime, endTime, category, state){
        return new Promise(function(fulfill, reject){
            var geoLocation;
            if(_.trim(state) != ""){
                geoLocation = 'US-'+_.toUpper(state);
            }else{
                geoLocation = 'US';
            }
            //console.log(`ByRegion: ${keywords}, ${startTime}, ${endTime}, ${category}, ${geoLocation}`);
            googleTrends.interestByRegion({keyword: keywords, startTime: new Date(startTime), endTime: new Date(endTime), geo: geoLocation, category : category})
            .then((res) => {
                console.log("\n Interest By Region");
               // console.log(res);
               fulfill(JSON.parse(res));
            })
            .catch((err) => {
                console.log(err);
                reject(err);
            })
        })
    }

    //Interest Over Time
    var getInterestOverTime = function(keywords, startTime, endTime, category, state){
        return new Promise(function(fulfill, reject){
            var geoLocation;
            if(_.trim(state) != ""){
                geoLocation = 'US-'+_.toUpper(state);
            }else{
                geoLocation = 'US';
            }
            //console.log(`ByTime: ${keywords}, ${startTime}, ${endTime}, ${category}, ${geoLocation}`);
            googleTrends.interestOverTime({keyword: keywords, startTime: new Date(startTime), endTime: new Date(endTime), geo: geoLocation, category : category})
            .then((res)=>{
                console.log("\n Interest Over Time");
                //console.log(res);
                fulfill(JSON.parse(res));
            }).catch((err)=>{
                console.log(err);
                reject(err);
            })
        }) 
    }

    //Related Queries
    var getRelatedQueries = function(keywords, startTime, endTime, category, state){
        return new Promise(function(fulfill, reject){
            var geoLocation;
            if(_.trim(state) != ""){
                geoLocation = 'US-'+_.toUpper(state);
            }else{
                geoLocation = 'US';
            }
            
            if(!(_.isArray(keywords))){
                var list = [];
                list.push(keywords);
                keywords = list;
            }
            //console.log(`${keywords}, ${startTime}, ${endTime}, ${category}, ${geoLocation}`);
            googleTrends.relatedQueries({keyword: keywords, startTime: new Date(startTime), endTime: new Date(endTime), geo: geoLocation, category : category})
            .then((res) => {
                console.log("\n Related Queries");
                // console.log(res);
                fulfill(JSON.parse(res));
            })
            .catch((err) => {
                console.error(err);
                reject(err);
            })
        })
    }

    //Related Topics
    var getRelatedTopics = function(keywords, startTime, endTime, category, state){
        return new Promise(function(fulfill, reject){
            var geoLocation;
            if(_.trim(state) != ""){
                geoLocation = 'US-'+_.toUpper(state);
            }else{
                geoLocation = 'US';
            }
            //console.log(`${keywords}, ${startTime}, ${endTime}, ${category}, ${geoLocation}`);
            googleTrends.relatedTopics({keyword: keywords, startTime: new Date(startTime), endTime: new Date(endTime), geo: geoLocation, category : category})
            .then((res) => {
                console.log("\n Related Topics");
                //console.log(res);
                fulfill(JSON.parse(res));
            })
            .catch((err) => {
                console.error(err);
                reject(err);
            })
        })
    }

    return {
        getInterestByRegion,
        getInterestOverTime,
        getRelatedQueries,
        getRelatedTopics
    }
}

module.exports = new Trends();