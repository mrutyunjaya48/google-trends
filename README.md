**Google Trends API Application**

** Run the app locally*

1. Clone the repo
`git clone `

2. Navigate into the clone project's root directory
`cd google-trends-equals3`

3. Run `npm install` to install the app's dependencies
4. Run `npm start` to start the app. 

###Apps logs will be saved into a local file called liveLogs.txt
5. Access the running app in a browser at

http://localhost:8080