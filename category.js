
var jsonfile = require('jsonfile');

function Category(){
    //var allCategories;
    var readAllCategories = function(){
        //fetch the categories json file
        var file = '/categories.json'
        jsonfile.readFile(file, function(err, obj) {
            //allCategories = obj;
            console.log(`All categories read: ${JSON.stringify(obj)}`);
            return obj;
        })
    }

    return{
        readAllCategories
    }
}

module.exports = new Category();